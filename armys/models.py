from django.db import models
from django.conf import settings


class Army(models.Model):
    army = models.CharField(max_length=200)
    list_name = models.CharField(max_length=150)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="armys",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
